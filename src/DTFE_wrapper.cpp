/*
 *  DTFE_wrapper - DTFE wrapper for e.g. RAMSES f90 interface
 *  Copyright (c) 2016 Boud Roukema, GPLv3 or later
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* C style includes */
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <gsl/gsl_rng.h>
#include <malloc.h>

/* C++ includes */

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string>
#include <boost/filesystem.hpp>
namespace bfs=boost::filesystem;

#include <vector>
#include "DTFE.h"
//#include "../../DTFE_include/define.h"
//#include "../../DTFE_include/particle_data.h"
//#include "../user_options.h"
#include "message.h"
#include "io/input_output.h"

using namespace std;


int DTFE_cplusplus_wrapper(/* INPUTS */
                           int64_t *noParticles_in,
                           double  *boxsize,
                           int32_t *dtfe_averaging_method,
                           int32_t *dtfe_n_gridsize,
                           double  *xp_in, /* positions */
                           double  *vp_in, /* velocities */
                           double  *mp_in, /* masses */
                           /* OUTPUTS */
                           User_options *userOptions,
                           Quantities *aQuantities
                     ){
  /*   User_options userOptions; */

  Read_data<Real> generateData;

  /* vector that keeps track of particle positions and their data */
  vector<Particle_data> particles;
  /* keep track of the sampling coordinates if the grid interpolation
     is done to a non-regular grid */
  vector<Sample_point> samplingCoordinates;

#ifdef TRIANGULATION
  DT delaunay_triangulation;
#endif

  char argvdummy[] = "xx";
  int i_dim; /* dimension iterator */
  int64_t i_part;

  /* minimum initialisation of userOptions object */
  userOptions->readOptions(1,(char **)(&argvdummy),false,false,true);

  /* hardwired cubical box, from 0.0 to *boxsize */
  for (i_dim=0; i_dim<NO_DIM; i_dim++){
    (userOptions->boxCoordinates)[2*i_dim] = 0.0;
    (userOptions->boxCoordinates)[2*i_dim+1] = *boxsize;
  };

  /* allocate memory for particle positions and velocities in "particles" structure */
  Real *positions = generateData.position(*noParticles_in);
  Real *velocities = generateData.velocity(*noParticles_in);

  /* Copy the position and velocity data to the "particles"
     structure.
     In f90: ordering: (x_1, x_2, x_3, ..., y_1, ... , z_1, ..., z_n);
     in dtfe: ordering: (x_1, y_1, z_1, x_2, y_2, z_2, ..., x_n, y_n, z_n);
  */

  for(i_part=0; i_part < (*noParticles_in); i_part++){
    for(i_dim=0; i_dim < NO_DIM; i_dim++){
      positions[NO_DIM*i_part + i_dim] =
        xp_in[NO_DIM*i_part + i_dim];
      /* xp_in[i_dim*(*noParticles_in) + i_part]; f90 */

      velocities[NO_DIM*i_part + i_dim] =
        vp_in[NO_DIM*i_part + i_dim];
      /* vp_in[i_dim*(*noParticles_in) + i_part]; f90 */
    };
  };


  userOptions->aField.velocity_gradient = true;
  userOptions->aField.velocity_divergence = true;
  userOptions->aField.velocity_IIinv = true;
  userOptions->aField.velocity_IIIinv = true;
  userOptions->aField.velocity = true;
  userOptions->gridSize.assign(3, *dtfe_n_gridsize);
  userOptions->periodic = true;
  /* averagingMethod */
  userOptions->method = *dtfe_averaging_method;
  userOptions->printOptions();

  generateData.transferData( &particles, &samplingCoordinates );

  Quantities uQuantities;     /* structure to store UNaveraged fields */
  /*  Quantities aQuantities; */    /* structure to store Averaged fields */

  /* Clear these in case this wrapper has been called before
     and deallocation not performed. Scope allocation rules
     should, in principle, make these clear() statements superfluous.
     However, it seems that in practice they are needed (boud 2017-03-13).
  */
  aQuantities->velocity_gradient.clear();
  aQuantities->velocity_divergence.clear();
  aQuantities->velocity_divergence_sigma.clear();
  aQuantities->velocity_IIinv.clear();
  aQuantities->velocity_IIinv_sigma.clear();
  aQuantities->velocity_IIIinv.clear();
  /* aQuantities->velocity_IIIinv_sigma.clear(); */ /* not yet implemented */
  aQuantities->velocity_Q.clear();
  aQuantities->velocity_Q_sigma.clear();
  aQuantities->velocity_shear_squared.clear();
  aQuantities->velocity_vorticity_squared.clear();
  aQuantities->density.clear();


  // main.cpp says "this function deletes the information in 'particles'"
  DTFE( &particles, samplingCoordinates,
        *userOptions,
        &uQuantities, aQuantities
#ifdef TRIANGULATION
        , delaunay_triangulation
#endif
        );

  return 0;
} /* int DTFE_cplusplus_wrapper */


extern "C" {
void dtfe_f90_wrapper_(/* INPUTS */
                     int64_t *noParticles_in,
                     double  *boxsize,
                     int64_t *dtfe_averaging_method,
                     int64_t *dtfe_n_gridsize,
                     double  **xp_in, /* positions */
                     double  **vp_in, /* velocities */
                     double  **mp_in, /* masses */
                     /* OUTPUTS */
                     /* These arrays must be allocated (and later
                        freed) by the calling routine*/
                     double  **dtfe_velocity, /* DT velocity Estimate */
                     double  **divergence, /* of vel gradient */
                     double  **IIinv, /* second invariant of vel gradient */
                     double  **IIIinv, /* third invariant of vel gradient */

                     double  **Q_kin_backreaction, /* kinematical backreaction map */
                     double  **Q_sigma, /* kinematical backreaction map */
                     double  **shear, /* scalar shear squared map */
                     double  **vorticity, /* scalar vorticity squared map */
                     double  **density /* density map */
                     ){

  User_options userOptions;

  Read_data<Real> generateData;

  /* vector that keeps track of particle positions and their data */
  vector<Particle_data> particles;
  /* keep track of the sampling coordinates if the grid interpolation
     is done to a non-regular grid */
  vector<Sample_point> samplingCoordinates;


#ifdef TRIANGULATION
  DT delaunay_triangulation;
#endif

  char argvdummy[] = "xx";
  int i_dim; /* dimension iterator */
  int64_t i_part;
  int64_t ip,jp,kp;
  int64_t ijk;

  /* minimum initialisation of userOptions object */
  userOptions.readOptions(1,(char **)(&argvdummy),false,false,true);

  /* hardwired cubical box, from 0.0 to *boxsize */
  for (i_dim=0; i_dim<NO_DIM; i_dim++){
    (userOptions.boxCoordinates)[2*i_dim] = 0.0;
    (userOptions.boxCoordinates)[2*i_dim+1] = *boxsize;
  };

  /* allocate memory for particle positions and velocities in "particles" structure */
  Real *positions = generateData.position(*noParticles_in);
  Real *velocities = generateData.velocity(*noParticles_in);

  /* Copy the position and velocity data to the "particles"
     structure. Ordering: (x_1, x_2, x_3, ..., y_1, ... , z_1, ..., z_n) */

#ifdef __GNUC__
  printf("malloc_usable_size(xp_in) = %lu malloc_usable_size(positions) = %lu  4*NO_DIM*(*noParticles_in) = %lu, *noParticles_in = %ld\n",
         malloc_usable_size(*xp_in), malloc_usable_size(positions), 4*NO_DIM*(*noParticles_in),
         *noParticles_in);
  printf("malloc_usable_size(vp_in) = %lu\n",
         malloc_usable_size(*vp_in));
#endif

  for(i_part=0; i_part < (*noParticles_in); i_part++){
    for(i_dim=0; i_dim < NO_DIM; i_dim++){
      positions[NO_DIM*i_part + i_dim] =
        (*xp_in)[i_dim*(*noParticles_in) + i_part];

      velocities[NO_DIM*i_part + i_dim] =
        (*vp_in)[i_dim*(*noParticles_in) + i_part];
    };
  };

  /* These may be useful to turn on. */
  /*
    userOptions.aField.velocity_gradient = true;
  */
  userOptions.aField.velocity = true;
  userOptions.aField.velocity_divergence = true;
  userOptions.aField.velocity_IIinv = true;
  userOptions.aField.velocity_IIIinv = true;
  userOptions.aField.velocity_Q = true;
  userOptions.aField.density = true;
  userOptions.gridSize.assign(3, *dtfe_n_gridsize);
  userOptions.periodic = true;
  /* averagingMethod */
  userOptions.method = *dtfe_averaging_method;
  userOptions.printOptions();

  generateData.transferData( &particles, &samplingCoordinates );

  Quantities uQuantities;     /* structure to store UNaveraged fields */
  Quantities aQuantities;     /* structure to store Averaged fields */

  /* Clear these in case this wrapper has been called before
     and deallocation not performed. */
  aQuantities.velocity.clear();
  aQuantities.velocity_gradient.clear();
  aQuantities.velocity_divergence.clear();
  aQuantities.velocity_divergence_sigma.clear();
  aQuantities.velocity_IIinv.clear();
  aQuantities.velocity_IIinv_sigma.clear();
  aQuantities.velocity_IIIinv.clear();
  aQuantities.velocity_Q.clear();
  aQuantities.velocity_Q_sigma.clear();
  aQuantities.velocity_shear_squared.clear();
  aQuantities.velocity_vorticity_squared.clear();
  aQuantities.density.clear();

  /* main.cpp warns that DTFE() deletes the information in 'particles'" */
  DTFE( &particles, samplingCoordinates,
        userOptions,
        &uQuantities, &aQuantities
#ifdef TRIANGULATION
        , delaunay_triangulation
#endif
        );

    size_t const *grid = &(userOptions.gridSize[0]);
    size_t const n_grid_total = grid[0]*grid[1]*grid[2];

    for (ip=0; (size_t)ip<grid[0]; ip++)
      for (jp=0; (size_t)jp<grid[1]; jp++)
        for(kp=0; (size_t)kp<grid[2]; kp++){
          ijk = ip*grid[1]*grid[2] + jp*grid[2] + kp;

          (*dtfe_velocity)[ijk] =
            (aQuantities.velocity[ijk])[0];
          (*dtfe_velocity)[n_grid_total + ijk] =
            (aQuantities.velocity[ijk])[1];
          (*dtfe_velocity)[2*n_grid_total + ijk] =
            (aQuantities.velocity[ijk])[2];

          (*divergence)[ijk] = aQuantities.velocity_divergence[ijk];
          (*IIinv)[ijk] = aQuantities.velocity_IIinv[ijk];
          (*IIIinv)[ijk] = aQuantities.velocity_IIIinv[ijk];
          (*Q_kin_backreaction)[ijk] = aQuantities.velocity_Q[ijk];
          (*Q_sigma)[ijk] = aQuantities.velocity_Q_sigma[ijk];
          (*shear)[ijk] = aQuantities.velocity_shear_squared[ijk];
          (*vorticity)[ijk] = aQuantities.velocity_vorticity_squared[ijk];
          (*density)[ijk] = aQuantities.density[ijk];

        };

    return;
} /* void DTFE_cplusplus_wrapper */
}
